<?php
//umask(0002);

if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
}

$app = include __DIR__ . '/../src/app.php';

$app->run(\Cloud\CloudFlareRequest::createFromGlobals());
