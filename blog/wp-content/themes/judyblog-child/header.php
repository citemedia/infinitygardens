<?php
/**
 * The Header for our theme.
 */
global $main_option;
$main_option = get_option('wope-main');
$social_option = get_option('wope-social');
$logo_width_half = $main_option['logo_retina_width'] / 2;
$logo_height_half = $main_option['logo_retina_height'] / 2;
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
  <!--<![endif]-->
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>
      <?php 
      if(is_front_page()){
	echo get_bloginfo('name') . ' | ' . get_bloginfo('description');
      }else{
	wp_title( '') ;
      }
      ?>
    </title>
    <?php if(!empty($main_option['favicon_url'])){?>
      <?php if(trim($main_option['favicon_url']) != ''){?>
	<link REL="icon" HREF="<?php echo $main_option['favicon_url'];?>">
      <?php }?>
    <?php }?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    
    <?php wp_head();?>
  </head>
  <?php
  $base_site = str_replace('/blog', '', get_site_url()) . '/residential';
  ?>
  <body <?php body_class(  ); ?>>
    <header class="last2">
      <div class="container_24">
        <div id="splitbtns">
          <div id="residentialbtn" class="splitbtn splitbtn_active">RESIDENTIAL</div>
          <a href="#" id="commercialbtn" class="splitbtn splitbtn_notactive">COMMERCIAL</a>
        </div>
        <div id="topheader">
          <article class="grid_6">
            <div class="inner-block"><h1><a class="residential_logo_big" id="top_logo" href="/blog">Infinity Gardens</a></h1></div>
          </article>
          <article class="grid_6 prefix_12 top_email fullscreen">
            <div class="inner-block">
              <span class="text1">1 403 401-0375</span>
              <div class="inner1">
                <span class="text2">E -mail:</span>
                <a class="link1" href="mailto:info@infinitygardens.ca" target="_top">info@infinitygardens.ca</a>
              </div>
            </div>
            <div class="clear"></div>
          </article>
          <article class="grid_8 prefix_10 top_email tablet_portrait">
            <div class="inner-block">
              <span class="text1">1 403 401-0375</span>
              <div class="inner1">
                <span class="text2">E -mail:</span>
                <a class="link1" href="mailto:info@infinitygardens.ca" target="_top">info@infinitygardens.ca</a>
              </div>
            </div>
            <div class="clear"></div>
          </article>
          <ul class="grid_18 mainmenu fullscreen">
            <li class=""><a href="<?php echo $base_site ?>/contact">CONTACT</a><div></div></li>
            <li class="activemenu"><a href="<?php echo bloginfo('url'); ?>">BLOG</a><div></div></li>
            <li class=""><a href="<?php echo $base_site ?>/services">SERVICES</a><div></div></li>
            <li class=""><a href="<?php echo $base_site ?>/testimonial">TESTIMONIAL</a><div></div></li>
            <li class=""><a href="<?php echo $base_site ?>/gallery">GALLERY</a><div></div></li>
            <li class=""><a href="<?php echo $base_site ?>/about">ABOUT US</a><div></div></li>
            <li class=""><a href="<?php echo $base_site ?>/">HOME</a></li>
          </ul>
          <ul class="grid_15 mainmenu mobile_portrait tablet_portrait">
            <li class=""><a href="<?php echo $base_site ?>/">HOME</a></li>
            <li class=""><a href="<?php echo $base_site ?>/about">ABOUT US</a></li>
            <li class=""><a href="<?php echo $base_site ?>/gallery">GALLERY</a></li>
            <li class=""><a href="<?php echo $base_site ?>/testimonial">TESTIMONIAL</a></li>
            <li class=""><a href="<?php echo $base_site ?>/services">SERVICES</a></li>
            <li class="activemenu"><a href="<?php echo bloginfo('url'); ?>">BLOG</a></li>
            <li class=""><a href="<?php echo $base_site ?>/contact">CONTACT</a></li>
          </ul>
          <div class="clear"></div>
        </div>

      </div>
      <div class="clear"></div>
    </header>
    <div id="background">
      <div id="main-menu-toggle">
	<div class="toggle-menu-top">
	  <span class="toggle-menu-close"><i class="fa fa-angle-left"></i></span>
	</div>
	<?php 
	if ( has_nav_menu('main-menu')){
	  wp_nav_menu( array( 'theme_location' => 'main-menu' ) );
	}else{
	?>
	  <ul>
	    <li ><a href="<?php echo home_url(); ?>"><?php _e('Home','wope');?></a></li>
	  </ul>
	<?php
	}
	?>  
	
      </div>
      <div id="back_top"><i class="fa fa-angle-up"></i></div>
      <div id="page" >
	<div id="header">
	  <div id="topbar">
	    <div class="wrap">
	      <div id="toggle-menu-button"><i class="fa fa-align-justify"></i></div>
	      <div class="main-menu">
		<?php 
		if ( has_nav_menu('main-menu')){
		  wp_nav_menu( array( 'theme_location' => 'main-menu' ) );
		}else{
		?>
		  <ul>
		    <li ><a href="<?php echo home_url(); ?>"><?php _e('Home','wope');?></a></li>
		  </ul>
		<?php
		}
		?> 
		<div class="cleared"></div>
	      </div><!-- End Main Menu -->
	      <?php if($social_option){?>
		<div class="top-social">
		  <?php foreach($social_option as $key => $each_social){?>
		    <?php if($key == 'rss'){?>
		      <?php if($each_social == 1){?>
			<span>
			  <a target="_blank" href=" <?php bloginfo( 'rss2_url' ); ?> "><i class="fa fa-rss"></i></a>
			</span>
		      <?php }?>
		      <?php }elseif(trim($each_social) != ''){?>
			<span><a target="_blank"  href="<?php echo $each_social;?>"><i class="fa fa-<?php echo $key;?>"></i></a></span>
		    <?php }?>
		  <?php }?>
		  <div class="cleared"></div>
		</div>
	      <?php }?>
	      <div class="cleared"></div>
	    </div>
	  </div>
	  <div class="wrap">
	    <div class="logo-box">
	      
	      <?php if(trim($main_option['logo_url']) != ''){?>
		<?php if(is_front_page()){?>
		  <h1>
		    <a class="logo-image" href="<?php echo home_url(); ?>">
		      <?php if( trim($main_option['logo_retina_url']) != ''){?>
			<img class="logo-normal" alt="<?php bloginfo('name');?>" src="<?php echo $main_option['logo_url'];?>">
			<image width="<?php echo $logo_width_half;?>" height="<?php echo $logo_height_half;?>" class="logo-retina" alt="<?php bloginfo('name');?>" src="<?php echo $main_option['logo_retina_url'];?>" >
		      <?php }else{?>
			<img alt="<?php bloginfo('name');?>" src="<?php echo $main_option['logo_url'];?>">
		      <?php }?>
		    </a>
		  </h1>
		<?php }else{?>
		  <a class="logo-image" href="<?php echo home_url(); ?>">
		    <?php if( trim($main_option['logo_retina_url']) != ''){?>
		      <img class="logo-normal" alt="<?php bloginfo('name');?>" src="<?php echo $main_option['logo_url'];?>">
		      <image width="<?php echo $logo_width_half;?>" height="<?php echo $logo_height_half;?>" class="logo-retina" alt="<?php bloginfo('name');?>" src="<?php echo $main_option['logo_retina_url'];?>" >
		    <?php }else{?>
		      <img alt="<?php bloginfo('name');?>" src="<?php echo $main_option['logo_url'];?>">
		    <?php }?>
		  </a>
		<?php }?>
	      <?php }else{?>
		<?php if(is_front_page()){?>
		  <h1>
		    <a class="logo-text" href="<?php echo home_url(); ?>">
		      <?php bloginfo('name');?>
		    </a>
		  </h1>
		  <?php if(get_bloginfo ( 'description' ) != ''){?>
		    <span class="logo-tagline"><?php echo get_bloginfo ( 'description' );?></span>
		  <?php }?>
		<?php }else{?>
		  <div>
		    <a class="logo-text" href="<?php echo home_url(); ?>">
		      <?php bloginfo('name');?>
		    </a>
		  </div>
		  <?php if(get_bloginfo ( 'description' ) != ''){?>
		    <span class="logo-tagline"><?php echo get_bloginfo ( 'description' );?></span>
		  <?php }?>
		<?php }?>
	      <?php }?>
	      
	    </div>

	    
	    <div class="cleared"></div>
	    <script>
	     var menu_open = false;
	     jQuery('#toggle-menu-button').click(function(){
	       if(menu_open == false){
		 jQuery("#main-menu-toggle").addClass("toggle-menu-open");
		 jQuery("#page").addClass("page-to-right");
		 menu_open = true;
	       }else{
		 jQuery("#main-menu-toggle").removeClass("toggle-menu-open");
		 jQuery("#page").removeClass("page-to-right");
		 menu_open = false;
	       }
	     });
	     
	     jQuery('.toggle-menu-close').click(function(){
	       if(menu_open == true){
		 jQuery("#main-menu-toggle").removeClass("toggle-menu-open");
		 jQuery("#page").removeClass("page-to-right");
		 menu_open = false;
	       }
	     });
	     
	    </script>
	  </div>
	</div><!-- End Header -->
