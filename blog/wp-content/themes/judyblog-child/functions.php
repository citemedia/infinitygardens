<?php

//load style in front end
function inf_load_style_frontend(){

    wp_register_style( 'skeleton', get_stylesheet_directory_uri() .'/infcss/skeleton.css');
	
	wp_enqueue_style('skeleton');
}

add_action('wp_enqueue_scripts', 'inf_load_style_frontend');