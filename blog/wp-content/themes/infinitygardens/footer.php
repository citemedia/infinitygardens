	<div class="clearboth"><!-- --></div>

  <footer class="container_24">
    <div id="bbb"><img src="/web/images/bbb.png" alt=""/></div>
    <div class="footer footerline">© 2014 Infinity Gardens. All rights reserved.</div>
    <div style="clear:right;"></div>
    <div class="footer footerline">Website by <a class="maverickdark" href="http://maverickagency.ca/"></a></div>
  </footer>

</div>

<?php wp_footer(); ?>

</body>
</html>
