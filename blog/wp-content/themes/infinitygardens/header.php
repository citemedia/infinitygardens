<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

  <head>
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

    <title><?php wp_title('//', true, 'right'); ?> <?php bloginfo('name'); ?></title>

    <meta name="description" content="<?php bloginfo('description'); ?>" />

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/skeleton.css" type="text/css"/>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/colours.css" type="text/css"/>
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
    <link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>

    <?php if (is_singular()) wp_enqueue_script('comment-reply'); ?>

    <?php wp_head(); ?>
  </head>
  <?php
  $base_site = str_replace('/blog', '', get_site_url()) . '/residential';
  ?>
  <body id="top" class="<?php echo setColourScheme(); ?>">

    <header class="last2">
      <div class="container_24">
        <div id="splitbtns">
          <div id="residentialbtn" class="splitbtn splitbtn_active">RESIDENTIAL</div>
          <a href="#" id="commercialbtn" class="splitbtn splitbtn_notactive">COMMERCIAL</a>
        </div>
        <div id="topheader">
          <article class="grid_6">
            <div class="inner-block"><h1><a class="residential_logo_big" id="top_logo" href="/blog">Infinity Gardens</a></h1></div>
          </article>
          <article class="grid_6 prefix_12 top_email fullscreen">
            <div class="inner-block">
              <span class="text1">1 403 401-0375</span>
              <div class="inner1">
                <span class="text2">E -mail:</span>
                <a class="link1" href="mailto:info@infinitygardens.ca" target="_top">info@infinitygardens.ca</a>
              </div>
            </div>
            <div class="clear"></div>
          </article>
          <article class="grid_8 prefix_10 top_email tablet_portrait">
            <div class="inner-block">
              <span class="text1">1 403 401-0375</span>
              <div class="inner1">
                <span class="text2">E -mail:</span>
                <a class="link1" href="mailto:info@infinitygardens.ca" target="_top">info@infinitygardens.ca</a>
              </div>
            </div>
            <div class="clear"></div>
          </article>
          <ul class="grid_17 mainmenu fullscreen">
            <li class=""><a href="<?php echo $base_site ?>/contact">CONTACT</a><div></div></li>
            <li class="activemenu"><a href="<?php echo bloginfo('url'); ?>">BLOG</a><div></div></li>
            <li class=""><a href="<?php echo $base_site ?>/services">SERVICES</a><div></div></li>
            <li class=""><a href="<?php echo $base_site ?>/testimonial">TESTIMONIAL</a><div></div></li>
            <li class=""><a href="<?php echo $base_site ?>/gallery">GALLERY</a><div></div></li>
            <li class=""><a href="<?php echo $base_site ?>/about">ABOUT US</a><div></div></li>
            <li class=""><a href="<?php echo $base_site ?>/">HOME</a></li>
          </ul>
          <ul class="grid_15 mainmenu mobile_portrait tablet_portrait">
            <li class=""><a href="<?php echo $base_site ?>/">HOME</a></li>
            <li class=""><a href="<?php echo $base_site ?>/about">ABOUT US</a></li>
            <li class=""><a href="<?php echo $base_site ?>/gallery">GALLERY</a></li>
            <li class=""><a href="<?php echo $base_site ?>/testimonial">TESTIMONIAL</a></li>
            <li class=""><a href="<?php echo $base_site ?>/services">SERVICES</a></li>
            <li class="activemenu"><a href="<?php echo bloginfo('url'); ?>">BLOG</a></li>
            <li class=""><a href="<?php echo $base_site ?>/contact">CONTACT</a></li>
          </ul>
          <div class="clear"></div>
        </div>

      </div>
      <div class="clear"></div>
    </header>

    <div class="container_24">

      <div id="header">

        <div class="blog-name"><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></div>
        <div class="description"><?php bloginfo('description'); ?></div>

      </div>