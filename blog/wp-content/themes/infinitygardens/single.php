<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<div class="post-info grid_8 alpha">

				<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>

				<div class="timestamp"><?php the_time('F j, Y'); ?> <!-- by <?php the_author() ?> --></div> <div class="comment-bubble"><a href="#comments"><?php comments_number('0', '1', '%'); ?></a></div>
				<div class="clearboth"><!-- --></div>

				<p><?php edit_post_link('Edit this entry', '', ''); ?></p>

			</div>


			<div class="post-content grid_16 omega">
				<?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>

				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

			</div>

			<div class="clearboth"><!-- --></div>

				<div class="post-meta-data"><span class="grid_3 alpha">Tags </span><span class="grid_18 omega"><?php the_tags('', ', ', ''); ?></span><div class="clear"></div></div>

				<div class="post-meta-data"><span class="grid_3 alpha">Categories </span><span class="grid_18 omega"><?php the_category(', ') ?></span><div class="clear"></div></div>
        
        <div class="post-meta-data"></div>

		</div>

	<?php //comments_template(); ?>

	<!-- <?php trackback_rdf(); ?> -->

	<?php endwhile; else: ?>

		<p>Sorry, no posts matched your criteria.</p>

<?php endif; ?>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
