<?php

function create_galleries()
{
    $gallery_dirs = array_filter(glob('web/content/galleries/*'), 'is_dir');
    sort($gallery_dirs);
    $galleries = [];
    foreach ($gallery_dirs as $dir) {
        $images = array_map(function($image){return basename($image);}, array_filter(glob($dir.'/image*'), 'is_file'));
        
        $dirname = trim(basename($dir), " \t\n\r\0\x0B1234567890");
        $parts = array_map('trim', explode('::', $dirname));
        if (!isset($parts[1])) {
          $parts[1] = '';
        }
        
        $descfile = realpath($dir.'/description.txt');
        if ($descfile) {
          $desc = file_get_contents($descfile);
        } else {
          $desc = '';
        }
        
        $gallery = ['titles_array' => $parts, 'base' => '/'.$dir, 'images' => $images, 'description' => $desc];
        $galleries[] = $gallery;
    }
    
    return $galleries;
}

function create_commercial_galleries()
{
    $gallery_dirs = array_filter(glob('web/content/commercial_galleries/*'), 'is_dir');
    sort($gallery_dirs);
    $galleries = [];
    foreach ($gallery_dirs as $dir) {
        $images = array_map(function($image){return basename($image);}, array_filter(glob($dir.'/image*'), 'is_file'));
        
        $dirname = trim(basename($dir), " \t\n\r\0\x0B1234567890");
        $parts = array_map('trim', explode('::', $dirname));
        if (!isset($parts[1])) {
          $parts[1] = '';
        }
        
        $descfile = realpath($dir.'/description.txt');
        if ($descfile) {
          $desc = file_get_contents($descfile);
        } else {
          $desc = '';
        }
        
        $gallery = ['titles_array' => $parts, 'base' => '/'.$dir, 'images' => $images, 'description' => $desc];
        $galleries[] = $gallery;
    }
    
    return $galleries;
}