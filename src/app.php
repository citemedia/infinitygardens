<?php

use Silex\Application;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\FormServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Silex\Provider\MonologServiceProvider;
use Monolog\Logger;

define('ROOT', __DIR__ . '/..');

require_once ROOT . "/vendor/autoload.php";

$app = new Application();

// turn on debug
if (php_sapi_name() === 'cli' || $_SERVER['REMOTE_ADDR'] === '127.0.0.1') {
  $app['dev'] = $app['debug'] = true;
  error_reporting(E_ALL);
  ini_set('display_errors', '1');
} else {
  $app['dev'] = $app['debug'] = false;
}
// force dev mode
$app['dev'] = $app['debug'] = true;

//$app->register(new MonologServiceProvider(), [
//    'monolog.logfile' => ROOT . "/logs/dev.log",
//    'monolog.name' => 'silex',
//    'monolog.level' => Logger::ERROR
//]);
$app->register(new Silex\Provider\SessionServiceProvider(), [
    'session.test' => php_sapi_name() === 'cli'
]);

$app->register(new Silex\Provider\TwigServiceProvider(), [
    'twig.path' => [ROOT . "/views"],
    'twig.options' => [
        'cache' => ROOT . "/cache/twig",
        'auto_reload' => true]]);

$app->register(new FormServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new UrlGeneratorServiceProvider);
$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('en'),
));

require 'routes/main.php';

//$app->register(new \Silex\Provider\DoctrineServiceProvider(), array(
//    'db.options' => $app['config']['database']
//));
//$app['security.firewalls'] = array(
//    'subscribed' => array(
//        'anonymous' => true,
//        'pattern' => '^.*$',
//        'form' => array(
//            'login_path' => '/login',
//            'check_path' => '/login-check',
//            'username_parameter' => LoginForm::NAME . '[email]',
//            'password_parameter' => LoginForm::NAME . '[password]',
//            'default_target_path' => 'homepage1'),
//        'logout' => array('logout_path' => '/logout'),
//        'users' => $app->share(function () use ($app) {
//            return new \Database\UserProvider($app['db']);
//        }),
//    ),
//);
//$app->register(new Silex\Provider\SecurityServiceProvider(), array(
//));
// intercept json request
$app->before(function (Request $request) {
  if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
    $data = json_decode($request->getContent(), true);
    $request->request->replace(is_array($data) ? $data : []);
  }
});

return $app;

