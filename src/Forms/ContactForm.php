<?php

namespace Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class ContactForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', 'text', [
                'label' => 'First name',
                'attr' => ['class' => '',
                    'placeholder' => 'First name',
                    'minlength' => 3],
                'constraints' => [new NotBlank()],
            ])
            ->add('lastname', 'text', [
                'label' => 'Last name',
                'attr' => ['class' => '',
                    'placeholder' => 'Last name',
                    'minlength' => 3],
                'constraints' => [new NotBlank()],
            ])
            ->add('email', 'text', [
                'label' => 'Email',
                'attr' => ['class' => '',
                    'placeholder' => 'Email'],
                'constraints' => [new NotBlank()],
            ])
            ->add('phone', 'text', [
                'label' => 'Phone',
                'attr' => ['class' => '',
                    'placeholder' => 'Phone'],
                'constraints' => [new Length(['min'=>5])],
            ])
            ->add('message', 'textarea', [
                'label' => 'Message',
                'attr' => ['placeholder' => 'Message', 'class' => '']
            ]);
    }

    public function getName()
    {
        return 'ContactForm';
    }
}
