<?php

namespace Cloud;

class CloudFlareRequest extends \Symfony\Component\HttpFoundation\Request
{
    public function isSecure()
    {
        return (
            (strtolower($this->headers->get('CF_VISITOR')) == '{"scheme":"https"}')
            ||
            parent::isSecure()
        );
    }

    /**
     * Override to always return the standard ports
     */
    public function getPort()
    {
        return $this->isSecure() ? 443 : 80;
    }

    /**
     * This is difference from parent::getUri() is that this implementation
     * does not include the port number.  This is because varnish:80 -> nginx:82
     * ends up with host:82 returned by parent::getUri()
     *
     * Generates a normalized URI for the Request.
     *
     * @return string A normalized URI for the Request
     *
     * @see getQueryString()
     *
     * @api
     */
    public function getUri()
    {
        $qs = $this->getQueryString();
        if (null !== $qs) {
            $qs = '?'.$qs;
        }

        $auth = '';
        if ($user = $this->getUser()) {
            $auth = $user;
        }

        if ($pass = $this->getPassword()) {
           $auth .= ":$pass";
        }

        if ('' !== $auth) {
           $auth .= '@';
        }

        return $this->getScheme().'://'.$auth.$this->getHost().$this->getBaseUrl().$this->getPathInfo().$qs;
    }
}
