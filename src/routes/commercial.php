<?php

use Symfony\Component\HttpFoundation\Request;

$commercial = $app['controllers_factory'];

$commercial->match('/', function (Request $request) use ($app) {
    return $app['twig']->render('commercial/index.twig');
})->bind('commercial');

$commercial->match('/about', function (Request $request) use ($app) {
    return $app['twig']->render('commercial/about.twig');
})->bind('commercial_about');

$commercial->match('/gallery', function (Request $request) use ($app) {
    $galleries = create_commercial_galleries();
    return $app['twig']->render('commercial/gallery.twig', ['galleries' => $galleries]);
})->bind('commercial_gallery');

$commercial->match('/testimonial', function (Request $request) use ($app) {
    return $app['twig']->render('commercial/testimonial.twig');
})->bind('commercial_testimonial');

$commercial->match('/services', function (Request $request) use ($app) {
    return $app['twig']->render('commercial/services.twig');
})->bind('commercial_services');

$commercial->match('/contact', function (Request $request) use ($app) {
    $form = $app['form.factory']
        ->createBuilder(new Forms\ContactForm())
        ->getForm();

    $dest = $request->get('destination_email');
    if (!$dest) {
        $dest = 'info@infinitygardens.com';
    }
    
    if ('POST' == $request->getMethod()) {
        $form->submit($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $message = \Swift_Message::newInstance()
                ->setSubject('Infinity Gardens contact form.')
                ->setFrom('contact@infinitygardens.com')
                ->setTo($dest)
                ->setBody($app['twig']->render('commercial/emails/contact.twig', [
                    'firstname' => $data['firstname'],
                    'lastname' => $data['lastname'],
                    'email' => $data['email'],
                    'phone' => $data['phone'],
                    'message' => $data['message']
            ]));
            $app['mailer']->send($message);
            
            $app['session']->getFlashBag()->add('contactform', 'Your message has been sent.');
            return $app->redirect($app['url_generator']->generate('commercial_contact'));
        }
    }

    return $app['twig']->render('commercial/contact.twig', array(
            'form' => $form->createView()
    ));
})->bind('commercial_contact');

return $commercial;