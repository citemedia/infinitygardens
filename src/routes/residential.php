<?php

use Symfony\Component\HttpFoundation\Request;

include 'src/functions.php';

$residential = $app['controllers_factory'];

$residential->match('/', function (Request $request) use ($app) {
    return $app['twig']->render('residential/index.twig');
})->bind('residential');

$residential->match('/about', function (Request $request) use ($app) {
    return $app['twig']->render('residential/about.twig');
})->bind('residential_about');

$residential->match('/gallery', function (Request $request) use ($app) {
    $galleries = create_galleries();
    return $app['twig']->render('residential/gallery.twig', ['galleries' => $galleries]);
})->bind('residential_gallery');

$residential->match('/testimonial', function (Request $request) use ($app) {
    return $app['twig']->render('residential/testimonial.twig');
})->bind('residential_testimonial');

$residential->match('/services', function (Request $request) use ($app) {
    return $app['twig']->render('residential/services.twig');
})->bind('residential_services');

$residential->match('/contact', function (Request $request) use ($app) {
    $form = $app['form.factory']
        ->createBuilder(new Forms\ContactForm())
        ->getForm();

    $dest = $request->get('destination_email');
    if (!$dest) {
        $dest = 'info@infinitygardens.ca';
    }
    
    if ('POST' == $request->getMethod()) {
        $form->submit($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $message = \Swift_Message::newInstance()
                ->setSubject('Infinity Gardens contact form.')
                ->setFrom('contact@infinitygardens.com')
                ->setTo($dest)
                ->setBody($app['twig']->render('residential/emails/contact.twig', [
                    'firstname' => $data['firstname'],
                    'lastname' => $data['lastname'],
                    'email' => $data['email'],
                    'phone' => $data['phone'],
                    'message' => $data['message']
            ]));
            $app['mailer']->send($message);
            
            $app['session']->getFlashBag()->add('contactform', 'Your message has been sent.');
            return $app->redirect($app['url_generator']->generate('residential_contact'));
        }
    }

    return $app['twig']->render('residential/contact.twig', array(
            'form' => $form->createView()
    ));
})->bind('residential_contact');

return $residential;
