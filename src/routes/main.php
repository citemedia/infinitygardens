<?php

use Symfony\Component\HttpFoundation\Request;

$residential = require 'residential.php';
$commercial = require 'commercial.php';

//$app->match('/', function (Request $request) use ($app) {
//    return $app['twig']->render('landing.twig');
//})->bind('landing');

$app->match('/', function (Request $request) use ($app) {
    return $app['twig']->render('residential/index.twig');
})->bind('landing');

$app->mount('/residential', $residential);
$app->mount('/commercial', $commercial);
